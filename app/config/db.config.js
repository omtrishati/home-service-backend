const env = require('./env');

const Sequelize = require('sequelize');
const sequelizeConnection = new Sequelize(env.database, env.username, env.password, {
    host: env.host,
    dialect: env.dialect,
    operatorAliases: false,
    pool: {
        max: env.max,
        min: env.min,
        acquire: env.pool.acquire,
        idle: env.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelizeConnection = sequelizeConnection;

module.exports = db;