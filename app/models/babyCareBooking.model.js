module.exports = (sequelizeConnection, Sequelize) => {
    const babyCareBooking = sequelizeConnection.define('babyCareBooking', {
        id: { type: Sequelize.UUID, primaryKey: true, allowNull: false, unique: true, defaultValue: Sequelize.UUIDV4 },
        bookingStatus: { type: Sequelize.STRING(20) }
    },
        {
            underscored: true
        })

    return babyCareBooking;
}