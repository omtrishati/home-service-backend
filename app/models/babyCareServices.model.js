// const { sequelizeConnection, Sequelize } = require("../config/db.config");

module.exports = (sequelizeConnection, Sequelize) => {
    const babyCareServices = sequelizeConnection.define('babyCareServices', {
        id: { type: Sequelize.UUID, primaryKey: true, allowNull: false, unique: true, defaultValue: Sequelize.UUIDV4 },
        availabilityStatus: { type: Sequelize.ENUM, values: ['FULLTIME', 'PARTTIME'] },
        primaryServices: { type: Sequelize.ENUM, values: ['BABYCARE'] },
        extraServices: { type: Sequelize.STRING(20) },
        rating: { type: Sequelize.STRING(10) }
    },
        {
            underscored: true
        })

    return babyCareServices;
}