const db = require("../config/db.config");
const { sequelizeConnection, Sequelize } = require("../config/db.config");

module.exports = (sequelizeConnection, Sequelize) => {
    const client = db.sequelizeConnection.define('client', {
        id: { type: Sequelize.INTEGER, primaryKey: true, allowNull: false, unique: true, defaultValue: Sequelize.UUIDV4 }
    })

    return client;
}