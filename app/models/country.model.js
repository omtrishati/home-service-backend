module.exports = (sequelizeConnection, Sequelize) => {
    const country = sequelizeConnection.define('country', {
        id: { type: Sequelize.INTEGER, primaryKey: true, allowNull: false, unique: true, autoIncrement: true },
        country_code: Sequelize.STRING(50),
        country_name: Sequelize.STRING(50),
    }, {
        underscored: true,
    })

    return country;
}