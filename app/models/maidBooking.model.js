// const db = require("../config/db.config");
// const { sequelizeConnection } = require("../config/db.config");

module.exports = (sequelizeConnection, Sequelize) => {
    const maidBooking = sequelizeConnection.define('maidBooking', {
        id: { type: Sequelize.UUID, primaryKey: true, allowNull: false, unique: true, defaultValue: Sequelize.UUIDV4 },
        bookingStatus: { type: Sequelize.STRING(20) }
    },
        {
            underscored: true
        })

    return maidBooking;
}