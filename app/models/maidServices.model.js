// const db = require("../config/db.config");
// const { sequelizeConnection, Sequelize } = require("../config/db.config");

module.exports = (sequelizeConnection, Sequelize) => {

    const maidServices = sequelizeConnection.define('maidServices', {
        id: { type: Sequelize.UUID, primaryKey: true, allowNull: false, unique: true, defaultValue: Sequelize.UUIDV4 },
        availabilityStatus: { type: Sequelize.ENUM, values: ['FULLTIME', 'PARTTIME'] },
        primaryServices: { type: Sequelize.ENUM, values: ['BROOMING', 'WASHING', 'MOPPING'] },
        extraServices: { type: Sequelize.STRING(20) },
        rating: { type: Sequelize.STRING(10) }
    },
        {
            underscored: true
        })

    return maidServices;
}