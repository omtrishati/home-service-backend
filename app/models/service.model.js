

module.exports = (sequelizeConnection, Sequelize) => {
    const service = sequelizeConnection.define('service', {
        id: { type: Sequelize.INTEGER, allowNull: false, primaryKey: true, unique: true, autoIncrement: true },
        service_type: Sequelize.STRING,
        active: { type: Sequelize.BOOLEAN }

    });
    // service.associate = function (models) {
    //     service.belongsTo(models.Company)
    // }
    return service;
}