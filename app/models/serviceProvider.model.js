const { sequelizeConnection, Sequelize } = require("../config/db.config");
const db = require("../config/db.config");

module.exports = (sequelizeConnection, Sequelize) => {
    const serviceProvider = db.sequelizeConnection.define('serviceProvider', {
        id: { type: Sequelize.UUID, allowNull: false, primaryKey: true, unique: true, defaultValue: Sequelize.UUIDV4 },
        services_provided: { type: Sequelize.STRING }
    });

    serviceProvider.associate=models=>{
        serviceProvider.hasOne(models.service)
    }
    return serviceProvider;
}