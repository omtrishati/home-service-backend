

module.exports = (sequelizeConnection, Sequelize) => {
    const state = sequelizeConnection.define('state', {
        id: { type: Sequelize.INTEGER, primaryKey: true, allowNull: false, unique: true, autoIncrement: true },
        state_code: Sequelize.STRING(50),
        state_name: Sequelize.STRING(50),
    }, {
        underscored: true,
    })

    return state;
}