// const { sequelizeConnection, Sequelize } = require("../config/db.config");

// const db = require('../config/db.config')
// let sequelizeConnection = db.sequelizeConnection;
// let Sequelize = db.Sequelize

module.exports = (sequelizeConnection, Sequelize) => {
    const user_info = sequelizeConnection.define('userInfo', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            allowNull: false,
            unique: true,
            defaultValue: Sequelize.UUIDV4
        },
        username: {
            type: Sequelize.STRING(20),
            allowNull: false,
            unique: true,

        },
        usertype: {
            type: Sequelize.STRING(20),
            allowNull: false,
        },
        password: {
            type: Sequelize.STRING(20),
            allowNull: false,

        }

    }, {
        underscored: true,

    })

    return user_info;
}