

module.exports = (sequelizeConnection, Sequelize) => {
    const userProfile = sequelizeConnection.define('userProfile', {
        id: {
            type: Sequelize.UUID,
            primaryKey: true,
            allowNull: false,
            unique: true,
            defaultValue: Sequelize.UUIDV4,
        },
        name: {
            type: Sequelize.STRING(50),
            allowNull: false
        },
        phone_primary: Sequelize.STRING(20),
        phone_secondary: Sequelize.STRING(20),
        address_primary: Sequelize.STRING(100),
        address_secondary: Sequelize.STRING(100),
        email: Sequelize.STRING(20),
        id_proof_primary: Sequelize.STRING(20),
        id_primary: Sequelize.BLOB,
        id_roof_secondary: Sequelize.STRING(20),
        id_secondary: Sequelize.BLOB,
        city: Sequelize.STRING(20),
    }, {
        underscored: true
    })

    return userProfile;
}